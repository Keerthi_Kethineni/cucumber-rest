package com.tekarch.cucumber_REST;

import java.util.HashMap;
import java.util.Map;

import org.junit.Assert;

import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import static com.jayway.restassured.RestAssured.*;
import static org.hamcrest.Matchers.*;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.path.json.JsonPath;
import com.jayway.restassured.response.Response;

public class StepDefinition {

	ObjectMapper mapper = new ObjectMapper();
	String url;
	Response response;
	static String landlordId;
	 

	@Given("^LandLordAPI: \"([^\"]*)\" is Up and Running$")
	public void landlordapi_is_Up_and_Running(String url) throws Throwable {
		this.url = url;
		response = given().baseUri(url).get();

		Assert.assertEquals(response.getStatusCode(), 200);
	}

	@When("^user performs a Post Request with below details$")
	public void user_performs_a_Post_Request_with_below_details(DataTable landLordDetails) throws Throwable {
		Map map = landLordDetails.asMap(String.class, String.class);

		String jsonRequest = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(map);

		response = given().baseUri(url).contentType(ContentType.JSON).body(jsonRequest).post();
	}

	@Then("^the Post Response code should be (\\d+) and user will get the LandLord details$")
	public void the_Post_Response_code_should_be_and_user_will_get_the_LandLord_details(int statusCode)
			throws Throwable {
		Assert.assertEquals(response.getStatusCode(), statusCode);

		Map<String, Object> map = mapper.readValue(response.body().asString(), Map.class);

		this.landlordId = (String) map.get("id");
		Assert.assertEquals((String) map.get("firstName"), "Keerthi");
		Assert.assertEquals((String) map.get("lastName"), "Kethineni");

	}

	@When("^user performs a GET Request$")
	public void user_performs_a_GET_Request() throws Throwable {
		response = given().baseUri(url).get();

	}

	@Then("^the Get Response code should be (\\d+) and user will get the LandLord details$")
	public void the_Get_Response_code_should_be_and_user_will_get_the_LandLord_details(int statusCode)
			throws Throwable {
		Assert.assertEquals(response.getStatusCode(), statusCode);
	}

	@Given("^the ID of LandLord$")
	public void the_ID_of_LandLord() throws Throwable {
		Assert.assertNotNull(this.landlordId);
	}

	@When("^user Upadte the LandLord firstName: \"([^\"]*)\" and lastname: \"([^\"]*)\"$")
	public void user_Upadte_the_LandLord_firstName_and_lastname(String firstName, String lastName) throws Throwable {

		Map<String, String> map = new HashMap<String, String>();
		map.put("firstName", firstName);
		map.put("lastName", lastName);
		String jsonRequest = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(map);

		response = given().baseUri(url).contentType(ContentType.JSON).pathParam("id", landlordId).body(jsonRequest).put("/{id}");

	}

	@Then("^the Put Response code should be (\\d+) with message: LandLord is successfully Updated$")
	public void the_Put_Response_code_should_be_with_message_LandLord_is_successfully_Updated(int statusCode)
			throws Throwable {
		Assert.assertEquals(response.getStatusCode(), statusCode);
		ObjectMapper mapper = new ObjectMapper();
		Map<String, Object> map = mapper.readValue(response.body().asString(), Map.class);

		Assert.assertTrue(((String) map.get("message")).contains("successfully updated"));
	}

	@When("^user retrieve the ID of LandLord$")
	public void user_retrieve_the_ID_of_LandLord() throws Throwable {
		Assert.assertNotNull(this.landlordId);
	}

	@When("^user performs the Delete Request$")
	public void user_performs_the_Delete_Request() throws Throwable {
		response = given().baseUri(url).pathParam("id", landlordId).delete("/{id}");
	}

	@Then("^the Delete Response code should be (\\d+) and with message: LandLord is successfully Deleted$")
	public void the_Delete_Response_code_should_be_and_with_message_LandLord_is_successfully_Deleted(int statusCode)
			throws Throwable {
		Assert.assertEquals(response.getStatusCode(), statusCode);
		ObjectMapper mapper = new ObjectMapper();
		Map<String, Object> map = mapper.readValue(response.body().asString(), Map.class);

		Assert.assertTrue(((String) map.get("message")).contains("successfully deleted"));
	}

}
