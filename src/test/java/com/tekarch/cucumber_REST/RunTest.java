package com.tekarch.cucumber_REST;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)

@CucumberOptions(format = {"pretty", "html:target/CucumberReport", "json:target/CucumberReport/cucumber.json"},
		features = "/Users/sreenivaskuppuru/Documents/workspace/cucumber-REST/src/test/resources/Feature/REST.feature"
		,glue={"com.tekarch.cucumber_REST"},
		tags = {}
 
)
public class RunTest {

}
