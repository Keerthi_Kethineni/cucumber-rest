Feature: LandLord API Test Automation using Cucumber 

Scenario: Post LandLord 
	Given LandLordAPI: "http://localhost:8080/landlords" is Up and Running 
	When user performs a Post Request with below details 
		| firstName | Keerthi  |
		| lastName | Kethineni |
	Then the Post Response code should be 201 and user will get the LandLord details
	
	
Scenario: Get LandLord 
	Given LandLordAPI: "http://localhost:8080/landlords" is Up and Running 
	When user performs a GET Request 
	Then the Get Response code should be 200 and user will get the LandLord details 
	
Scenario: Update LandLord 
	Given LandLordAPI: "http://localhost:8080/landlords" is Up and Running 
	Given the ID of LandLord 
	When user Upadte the LandLord firstName: "Keetu" and lastname: "Kethis" 
	Then the Put Response code should be 200 with message: LandLord is successfully Updated 
	
	
Scenario: Delete LandLord 
	Given LandLordAPI: "http://localhost:8080/landlords" is Up and Running 
	When user retrieve the ID of LandLord 
	When user performs the Delete Request 
	Then the Delete Response code should be 200 and with message: LandLord is successfully Deleted 